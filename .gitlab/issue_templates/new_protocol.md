## Checklist

In order to create a new protocol please complete the following.

-   [ ] Attach a file with the protocol (docx, pdf, txt, md, etc.)
-   [ ] Make the title of the issue the name of the protocol.

Over time as we get a more standard list of things a good protocol has this list
will improve.

<!-- Don't remove below this line -->

/label ~protocol
/milestone %Protocols
/cc @suhana13 @sruthipsuresh
