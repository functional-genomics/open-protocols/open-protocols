<!-- CLICK "Preview" FOR INSTRUCTIONS IN A MORE READABLE FORMAT -->

## Prerequisites

-   We realize there is a lot of data requested here. We ask only that you do your best to provide as much information as possible so we can better help you.
-   Read the [contributing guidelines](../../CONTRIBUTING.md).
-   Ensure the issue isn't already reported.

_Delete the above section and the instructions in the sections below before submitting_

## Description

For bug reports, please provide as much _relevant_ info as possible.

### Error Message & Stack Trace

```
COPY THE ERROR MESSAGE, INCLUDING STACK TRACE HERE
```

### Command-Line Arguments

Copy your mkdocs build scripts or the `mkdocs` command used:

```
mkdocs [OPTIONS HERE]
```

## Relevant Links

-   **BONUS POINTS:** Create a [minimal reproduction](http://stackoverflow.com/help/mcve) and upload it to GitHub. This will get you the fastest support.

## Environment

**BONUS POINTS:**
Tell us which operating system you are using, as well as which versions of
conda, and mkdocs. Run the following to get it quickly:

```
conda -V
mkdocs -V
```
