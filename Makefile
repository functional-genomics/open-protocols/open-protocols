all: install build

install: poetry install-pre-commit
	poetry install

build:
	poetry run mkdocs build

serve:
	poetry run mkdocs serve

pre-commit: install-pre-commit
	poetry run pre-commit run --all-files

install-pre-commit:
	poetry run pre-commit install

poetry:
ifeq (true,true)
	curl -sSL "https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py" | python
else
	(Invoke-WebRequest -Uri https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py -UseBasicParsing).Content | python
endif

# Convenience aliases
i: install
pc: pre-commit
