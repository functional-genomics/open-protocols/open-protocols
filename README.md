# Open Protocols Documentation

## Getting Started

### Install

Make sure you have `make` installed

```shell
make -v
```

Then install(This installs `poetry`, the dependancies, and sets up `pre-commit`):

```shell
make install
```

#### Windows

There is a windows rule currently. However you'll need to change

```diff
# Makefile
poetry:
- ifeq (true,true)
+ ifeq (false,true)
	curl -sSL "https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py" | python
```

Until I figure out a more clever way of doing this.

### Run mkdocs locally

To work locally with this project, you'll have to follow the steps below:

1. Preview your project: `make serve`,
   your site can be accessed under `localhost:8000`
2. Add content

Read more at MkDocs [documentation](https://www.mkdocs.org/).

All of the commands can of course be run by hand, the `Makefile` is for your convenience.

## Contributing

Check [CONTRIBUTING](./CONTRIBUTING.md) for the Pull Request Process and Code of Conduct.

Some more general thoughts on how to run projects and keep this clean:

1. [Conventional Commits](https://www.conventionalcommits.org)
2. [Trunk based developement](https://trunkbaseddevelopment.com/)
3. [Semantic Versioning](https://semver.org/)
